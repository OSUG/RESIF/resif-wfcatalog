# Packaging de WFCatalog pour Résif

Ce projet permet de construire et publier les containers suivant pour l'infrastructure Résif :

* `resif-wfcatalog/worker` : le worker permettant d'indexer les nouvelles données qui transitent dans le pipeline AMQP. Voir le fichier `Docker.worker`

L'intégration continue construit et publie les images déployables en production.

Pour le webservice wfcatalog, l'image est générée par le projet github d'origine: https://github.com/EIDA/wfcatalog

### Mise à jour

```bash
git submodule update --remote --merge
git add wfcatalog
git commit -m "Mise à jour de wfcatalog upstream"
git push
```
  
L'intégration continue se charge de construire les containers et de les publier.



