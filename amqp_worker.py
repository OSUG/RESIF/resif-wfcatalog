#!/usr/bin/env python3

import json
import logging
import os

import pika

from wfcatalog.collector.WFCatalogCollector import WFCatalogCollector

SCRIPT_VERSION = "2024.302"

AMQP_SERVER = os.environ.get("AMQP_SERVER")
AMQP_PORT = os.environ.get("AMQP_PORT", 5672)
AMQP_VHOST = os.environ.get("RUNMODE")
AMQP_USER = os.environ.get("AMQP_USER")
AMQP_PASSWORD = os.environ.get("AMQP_PASSWORD")
AMQP_HEARTBEAT = os.environ.get("AMQP_HEARTBEAT", 600)
AMQP_TIMEOUT = os.environ.get("AMQP_TIMEOUT", 300)
WFCATALOG_EXCHANGE = os.environ.get("WFCATALOG_EXCHANGE", "post-integration")
WFCATALOG_QUEUE = os.environ.get("WFCATALOG_QUEUE", "wfcatalog")
WFCATALOG_ROUTING_KEY = os.environ.get("WFCATALOG_ROUTING_KEY", "miniseed")

# Logger configuration
logging.getLogger("WFCatalog Collector").setLevel(logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(level=logging.INFO)


def launch_wfcatalog_updates(channel, method, _, body):
    body = body.decode("utf-8")
    logger.info("Received %s:%s", method.routing_key, body)
    wfcollector = WFCatalogCollector(to_stdout=True)
    files = json.dumps(body.splitlines())
    wfcollector.process(
        {"update": True, "force": True, "csegs": True, "flags": True, "list": files}
    )

    # notify rabbitmq server that message has been handled
    channel.basic_ack(delivery_tag=method.delivery_tag)


def main():
    logger.info("Starting")
    logger.info("Trying to connect to %s/%s", AMQP_SERVER, WFCATALOG_QUEUE)
    credentials = pika.PlainCredentials(AMQP_USER, AMQP_PASSWORD)
    parameters = pika.ConnectionParameters(
        AMQP_SERVER,
        AMQP_PORT,
        AMQP_VHOST,
        credentials,
        heartbeat=AMQP_HEARTBEAT,
        blocked_connection_timeout=AMQP_TIMEOUT,
    )
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.basic_qos(prefetch_count=1)
    channel.exchange_declare(
        exchange=WFCATALOG_EXCHANGE, exchange_type="direct", durable=True
    )
    channel.queue_declare(queue=WFCATALOG_QUEUE, durable=True)
    channel.queue_bind(
        queue=WFCATALOG_QUEUE,
        exchange=WFCATALOG_EXCHANGE,
        routing_key=WFCATALOG_ROUTING_KEY,
    )

    logger.info("Start consuming the '%s' queue", WFCATALOG_QUEUE)
    channel.basic_consume(
        queue=WFCATALOG_QUEUE,
        on_message_callback=launch_wfcatalog_updates,
        auto_ack=False,
    )
    channel.start_consuming()
    logger.info("amqp worker quitting")


if __name__ == "__main__":
    main()
